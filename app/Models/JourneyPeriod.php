<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JourneyPeriod extends Model
{
    protected $fillable = ['started_at', 'finished_at', 'lat_start', 'lng_start', 'address_start', 'lat_end', 'lng_end', 'address_end'];
    public $timestamps = false;
    public $dates = ['started_at', 'finished_at'];

    public function user_journey()
    {
        return $this->belongsTo(UserJourney::class);
    }
}
