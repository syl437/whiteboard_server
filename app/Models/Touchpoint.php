<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Plank\Mediable\Mediable;

class Touchpoint extends Model
{
    use Mediable;

    protected $fillable = ['mark'];

    protected $appends = ['content', 'title', 'color'];

    public function user_journey()
    {
        return $this->belongsTo(UserJourney::class);
    }

    public function point()
    {
        return $this->belongsToMany(Point::class);
    }

    public function texts()
    {
        return $this->hasMany(Text::class);
    }

    public function touchpoint_periods()
    {
        return $this->hasMany(TouchpointPeriod::class);
    }

    public function getTitleAttribute()
    {
        $point = Point::find($this->point_id);
        if ($point){
            return $point->title;
        }
        return '';
    }

    public function getColorAttribute()
    {
        $point = Point::find($this->point_id);
        if ($point){
            return $point->color;
        }
        return '#FFFFFF';
    }

    public function getContentAttribute()
    {
        $content = collect([]);

        if ($this->texts()->count()){
            foreach ($this->texts as $text){
                $text->type = 'text';
                $content->push($text);
            }
        }

        $images = $this->getMedia('image');
        if ($images->count()){
            foreach ($images as $image){
                $image->fullUrl = $image->getUrl();
                $image->type = 'image';
                $content->push($image);
            }
        }

        $videos = $this->getMedia('video');
        if ($videos->count()){
            foreach ($videos as $video){
                $video->fullUrl = $video->getUrl();
                $video->type = 'video';
                $content->push($video);
            }
        }

        $audios = $this->getMedia('audio');
        if ($audios->count()){
            foreach ($audios as $audio){
                $audio->fullUrl = $audio->getUrl();
                $audio->type = 'audio';
                $content->push($audio);
            }
        }

        if ($content->count()){
            return $content->sortBy('created_at')->values()->all();
        }

        return [];
    }

    public function media()
    {
        return $this->morphToMany(config('mediable.model'), 'mediable')
            ->withPivot('tag', 'order', 'mediable_type')
            ->orderBy('order');
    }
}
