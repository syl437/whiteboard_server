<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Plank\Mediable\Mediable;

class TouchpointPeriod extends Model
{

    use Mediable;

    protected $fillable = ['started_at', 'finished_at', 'lat', 'lng', 'address', 'duration'];
    public $timestamps = false;
    public $dates = ['started_at', 'finished_at'];

    public function touchpoint()
    {
        return $this->belongsTo(Touchpoint::class);
    }

    public function texts()
    {
        return $this->hasMany(Text::class);
    }

    public function user_journey()
    {
        return $this->belongsTo(UserJourney::class);
    }
}
