<?php

namespace App\Http\Controllers\Web\V1;

use App\Models\Journey;
use App\Models\Point;
use App\Models\Touchpoint;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TouchpointController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // TODO: request

        $touchpoint = new Touchpoint();
        $touchpoint->fill($request->all());
        $touchpoint->user_journey_id = $request->get('user_journey_id');
        $touchpoint->point_id = $request->get('point_id');
        $touchpoint->save();

        return responder()->success($touchpoint);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Touchpoint $touchpoint)
    {
        $touchpoint->mark = $request->get('mark');
        $touchpoint->save();

        return responder()->success();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
