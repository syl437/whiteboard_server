<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Text;
use App\Models\Touchpoint;
use App\Models\TouchpointPeriod;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TextController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Touchpoint $touchpoint)
    {
        // TODO: request
        $text = new Text();
        $text->content = $request->get('content');
        $text->touchpoint()->associate($touchpoint);

        $touchpoint_period = TouchpointPeriod::where('touchpoint_id', $touchpoint->id)
            ->where('user_journey_id', $touchpoint->user_journey_id)
            ->orderByDesc('id')
            ->first();

        $text->touchpoint_period()->associate($touchpoint_period);

        $text->save();

        return responder()->success();
    }

}
