<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Journey;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class JourneyController extends Controller
{
    /**
     * Display a listing of the journeys.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user = auth()->guard('api')->user();
        app()->setLocale($user->language);

        $journeys = Journey::query()
            ->with(
                [
                    'points' => function ($q) {
                        $q
                            ->orderBy('pivot_weight', 'asc');
                    },
                    'my_weighted_points' => function ($q) {
                        $q
                            ->orderBy('pivot_weight', 'asc');
                    }
                ]
            )
            ->get();

        foreach ($journeys as $journey) {

            $journey->getTranslationsArray();

            foreach ($journey->translations as $tr) {
                if ($tr->locale === 'en') {
                    $journey->title_en = $tr->title;
                }
                if ($tr->locale === 'he') {
                    $journey->title_he = $tr->title;
                }
            }

            $journey->load(['user_journeys' => function($query) use ($user) {
                $query->where('user_id', $user->id);
            }]);
        }

        $journeys = $journeys->toArray();

        foreach ($journeys as &$journey) {

            if (count($journey['my_weighted_points']) > 0) {
                $journey['points'] = $journey['my_weighted_points'];
            }
        }

        return responder()->success($journeys)->respond();

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Journey $journey)
    {
        if ($request->has('order')) {

            $map = collect($request->get('order'))->mapWithKeys(function ($item, $index) {
                return [$item => [
                    'user_id' => auth()->user()->id,
                    'weight' => $index,
                ]];
            });

            $journey
                ->my_weighted_points()
                ->sync($map, true);

            return responder()->success();
        }
    }

}
