<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Journey;
use App\Models\Point;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PointController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // TODO: request

        $user = auth()->guard('api')->user();

        $point = new Point();
        $point->user()->associate($user);
        $point->color = '#FFFFFF';
        $point->save();

        $point->translateOrNew('en')->title = $request->get('title');
        $point->translateOrNew('he')->title = $request->get('title');
        $point->save();

        $journey = Journey::find($request->get('journey_id'));

        if ($journey->my_weighted_points()->count() == 0)
        {
            $map = $journey->points->mapWithKeys(function ($item) {
                return [$item['id'] => [
                    'user_id' => auth()->user()->id,
                    'weight' => $item['weight'] ?? 0,
                ]];
            })->toArray();

            $journey
                ->my_weighted_points()
                ->sync($map);

        }

        $max_weight = $journey->my_weighted_points()->max('journey_point.weight');

        $journey->my_weighted_points()->attach($point, ['user_id' => auth()->user()->id, 'weight' => $max_weight + 1]);

        return responder()->success();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Point $point)
    {

        if ($request->has('old_weight') && $request->has('weight') && $request->has('journey_id'))
        {
            $point->weight = $request->get('weight');

            $journey = Journey::with(['points' => function($q) {
                $q->wherePivot('user_id', auth()->user()->id)->orderBy('pivot_weight', 'asc');
            }])->find($request->get('journey_id'));

            if ($journey != null) {

                if ($journey->points->count() === 0) {

                    $original_journey = Journey::with(['points' => function($q) {
                        $q->whereNull('points.user_id')->orderBy('pivot_weight', 'asc');
                    }])->find($request->get('journey_id'));

                    if ($original_journey != null) {

                        foreach ($original_journey->points as $original_point) {
                            $journey->points()->attach($original_point->id, [
                                'user_id' => auth()->user()->id,
                                'weight' => $original_point->pivot->weight,
                            ]);
                        }
                    }

                }

                $journey = Journey::with(['points' => function($q) {
                    $q->wherePivot('user_id', auth()->user()->id)->orderBy('pivot_weight', 'asc');
                }])->find($request->get('journey_id'));

                if ($journey == null || $journey->points->count() === 0) {
                    return responder()->error()->respond();
                }

                $new_weight = $request->get('weight');

                $old_weight = $request->get('old_weight');

                if ($new_weight < $old_weight) {

                    foreach (Journey::with(['points' => function($q) use($old_weight, $new_weight) {
                        $q->wherePivot('user_id', auth()->user()->id)->whereBetween('journey_point.weight', [$new_weight, $old_weight - 1])->orderBy('pivot_weight', 'asc');
                    }])->find($request->get('journey_id'))->points as $p)
                    {
                        $journey->points()->wherePivot('user_id', '=', auth()->user()->id)->updateExistingPivot($p->id, [
                            'weight' => $p->pivot->weight + 1,
                        ]);
                    }

                } else {

                    foreach (Journey::with(['points' => function($q) use($old_weight, $new_weight) {
                        $q->wherePivot('user_id', auth()->user()->id)->whereBetween('journey_point.weight', [$old_weight + 1, $new_weight])->orderBy('pivot_weight', 'asc');
                    }])->find($request->get('journey_id'))->points as $p)
                    {
                        $journey->points()->wherePivot('user_id', '=', auth()->user()->id)->updateExistingPivot($p->id, [
                            'weight' => $p->pivot->weight - 1,
                        ]);
                    }

                }

                $journey->points()->wherePivot('user_id', '=', auth()->user()->id)->updateExistingPivot($point->id, [
                    'weight' => $new_weight,
                ]);

            }
        }

        return responder()->success();
    }

}
