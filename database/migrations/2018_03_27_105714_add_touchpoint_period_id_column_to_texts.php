<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTouchpointPeriodIdColumnToTexts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('texts', function (Blueprint $table) {
            $table->integer('touchpoint_period_id')->unsigned()->index()->nullable()->after('touchpoint_id');
            $table->foreign('touchpoint_period_id')->references('id')->on('touchpoint_periods')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('texts', function (Blueprint $table) {
            $table->dropForeign('texts_touchpoint_period_id_foreign');
            $table->dropColumn('touchpoint_period_id');
        });
    }
}
