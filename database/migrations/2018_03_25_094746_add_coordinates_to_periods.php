<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCoordinatesToPeriods extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('touchpoint_periods', function (Blueprint $table) {
            $table->string('lat')->nullable();
            $table->string('lng')->nullable();
            $table->string('address')->nullable();
        });

        Schema::table('journey_periods', function (Blueprint $table) {
            $table->string('lat_start')->nullable();
            $table->string('lng_start')->nullable();
            $table->string('address_start')->nullable();
            $table->string('lat_end')->nullable();
            $table->string('lng_end')->nullable();
            $table->string('address_end')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('touchpoint_periods', function (Blueprint $table) {
            $table->dropColumn('lat');
            $table->dropColumn('lng');
            $table->dropColumn('address');
        });

        Schema::table('journey_periods', function (Blueprint $table) {
            $table->dropColumn('lat_start');
            $table->dropColumn('lng_start');
            $table->dropColumn('address_start');
            $table->dropColumn('lat_end');
            $table->dropColumn('lng_end');
            $table->dropColumn('address_end');
        });
    }
}
