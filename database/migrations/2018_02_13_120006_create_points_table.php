<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('points', function (Blueprint $table) {
            $table->increments('id');
            $table->string('color')->default('#FFFFFF');
            $table->integer('weight');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();

        });

        Schema::create('point_translations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('point_id')->unsigned();
            $table->string('title');
            $table->string('locale')->index();
            $table->unique(['point_id','locale']);
            $table->foreign('point_id')->references('id')->on('points')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('point_translations');
        Schema::dropIfExists('points');
    }
}
