<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJourneysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('journeys', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });

        Schema::create('journey_translations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('journey_id')->unsigned();
            $table->string('title');
            $table->string('locale')->index();
            $table->unique(['journey_id','locale']);
            $table->foreign('journey_id')->references('id')->on('journeys')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('journey_translations');
        Schema::dropIfExists('journeys');
    }
}
