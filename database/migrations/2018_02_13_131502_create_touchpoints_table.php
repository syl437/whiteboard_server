<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTouchpointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('touchpoints', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_journey_id')->unsigned()->index();
            $table->foreign('user_journey_id')->references('id')->on('user_journeys')->onDelete('cascade');
            $table->integer('point_id')->unsigned()->index();
            $table->foreign('point_id')->references('id')->on('points')->onDelete('cascade');
            $table->integer('mark')->nullable();
            $table->integer('duration')->nullable();
            $table->string('lat')->nullable();
            $table->string('lng')->nullable();
            $table->string('address')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('touchpoints');
    }
}
