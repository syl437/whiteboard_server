<?php

use Illuminate\Database\Seeder;

class MediaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $media1 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "journeys/gas_station.jpg");
        $media2 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "journeys/coffee_place.jpg");
        $media3 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "journeys/train_station.jpg");
        $media4 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "journeys/book_store.jpg");

        $j1 = \App\Models\Journey::find(1);
        $j2 = \App\Models\Journey::find(2);
        $j3 = \App\Models\Journey::find(3);
        $j4 = \App\Models\Journey::find(4);

        $j1->attachMedia($media1, 'image');
        $j2->attachMedia($media2, 'image');
        $j3->attachMedia($media3, 'image');
        $j4->attachMedia($media4, 'image');

    }
}
