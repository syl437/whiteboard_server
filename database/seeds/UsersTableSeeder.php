<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user1 = new \App\Models\User();
        $user1->name = 'Evgenia Gofman';
        $user1->email = 'evgeniagofman@gmail.com';
        $user1->phone = '+972524208555';
        $user1->api_token = '123';
        $user1->language = 'en';
        $user1->save();

        $user2 = new \App\Models\User();
        $user2->name = 'Ray Bradbury';
        $user2->email = 'raybradbury@test.com';
        $user2->phone = '+972521111111';
        $user2->api_token = 'ray';
        $user2->save();

        $user4 = new \App\Models\User();
        $user4->name = 'Stephen King';
        $user4->email = 'stephenking@test.com';
        $user4->phone = '+972523333333';
        $user4->api_token = 'stephen';
        $user4->language = 'en';
        $user4->save();

        $user5 = new \App\Models\User();
        $user5->name = 'George R. R. Martin';
        $user5->email = 'georgemartin@test.com';
        $user5->phone = '+972524444444';
        $user5->api_token = 'george';
        $user5->save();

        $user9 = new \App\Models\User();
        $user9->name = 'Joan K. Rowling';
        $user9->email = 'joanrowling@test.com';
        $user9->phone = '+972528888888';
        $user9->api_token = 'joan';
        $user9->save();

    }
}
