<?php

use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new \App\Models\Admin();
        $admin->username = 'admin';
        $admin->password = 'admin';
        $admin->email = 'evgeniagofman@gmail.com';
        $admin->save();
    }
}
