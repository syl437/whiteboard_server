<?php

use Illuminate\Database\Seeder;

class JourneysSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $j1 = new \App\Models\Journey();
        $j1->translateOrNew('en')->title = 'Gas station';
        $j1->translateOrNew('he')->title = 'תחנת דלק';
        $j1->save();

        $j2 = new \App\Models\Journey();
        $j2->translateOrNew('en')->title = 'Cafe';
        $j2->translateOrNew('he')->title = 'בית קפה';
        $j2->save();

        $j3 = new \App\Models\Journey();
        $j3->translateOrNew('en')->title = 'Train station';
        $j3->translateOrNew('he')->title = 'תחנת רקבת';
        $j3->save();

        $j4 = new \App\Models\Journey();
        $j4->translateOrNew('en')->title = 'Book store';
        $j4->translateOrNew('he')->title = 'חנות ספרים';
        $j4->save();
    }
}
