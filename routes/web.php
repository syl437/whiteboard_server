<?php

//Route::get('/', function () {
//    return view('welcome');
//});

app('router')->group(['prefix' => 'v1'], function (\Illuminate\Routing\Router $r) {

    $r->post('tokens', 'Web\V1\TokenController@login');
    $r->get('admins/{admin}', 'Web\V1\TokenController@show');
    $r->post('admins/{admin}', 'Web\V1\TokenController@update');
    $r->get('passwords/last', 'Web\V1\TokenController@showPassword');
    $r->post('passwords', 'Web\V1\TokenController@storePassword');

    $r->resource('users', 'Web\V1\UserController', ['only' => ['index', 'store', 'show', 'update', 'destroy']]);
    $r->resource('journeys', 'Web\V1\JourneyController', ['only' => ['index', 'store', 'show', 'update', 'destroy']]);
    $r->post('user_journeys/{user_journey}/excel', 'Web\V1\UserJourneyController@sendExcel');
    $r->resource('user_journeys', 'Web\V1\UserJourneyController', ['only' => ['show']]);
    $r->resource('points', 'Web\V1\PointController', ['only' => ['index', 'store', 'show', 'update', 'destroy']]);
    $r->resource('touchpoints', 'Web\V1\TouchpointController', ['only' => ['index', 'store', 'show', 'update', 'destroy']]);
    $r->resource('touchpoints.texts', 'Web\V1\TextController', ['only' => ['index', 'store', 'show', 'update', 'destroy']]);
    $r->resource('media', 'Web\V1\MediaController', ['only' => ['index', 'store', 'show', 'update', 'destroy']]);

});