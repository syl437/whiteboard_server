<?php

use Illuminate\Routing\Router;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

app('router')->group(['prefix' => 'v1'], function (Router $r) {

    $r->post('tokens', 'Api\V1\TokenController@login');
    $r->get('lang', 'Api\V1\TokenController@changeLanguage');

});

app('router')->group(['middleware' => 'auth:api', 'prefix' => 'v1'], function (Router $r) {

    $r->resource('journeys', 'Api\V1\JourneyController', ['only' => ['index', 'update']]);
    $r->post('user_journeys/{user_journey}/touchpoint_periods', 'Api\V1\UserJourneyController@reportTouchpointPeriod');
    $r->post('user_journeys/{user_journey}/journey_periods', 'Api\V1\UserJourneyController@reportJourneyPeriod');
    $r->resource('user_journeys', 'Api\V1\UserJourneyController', ['only' => ['store', 'show']]);
    $r->resource('points', 'Api\V1\PointController', ['only' => ['store', 'update']]);
    $r->resource('touchpoints', 'Api\V1\TouchpointController', ['only' => ['store', 'update']]);
    $r->resource('touchpoints.texts', 'Api\V1\TextController', ['only' => ['store']]);
    $r->post('media/avatar', 'Api\V1\MediaController@storeAvatar');
    $r->delete('media/avatar/{media}', 'Api\V1\MediaController@deleteAvatar');
    $r->resource('touchpoints.media', 'Api\V1\MediaController', ['only' => ['store']]);
    $r->resource('users', 'Api\V1\UserController', ['only' => ['show', 'update']]);

});